import re
from itertools import permutations
"""Overall Strategy:
    Compute all pairs shortest path for the starting room and all rooms with positive flow rate.
    Find the vest order to open the valves.
"""


class Room:
    def __init__(self, name, flow_rate, tunnels) -> None:
        self.name = name
        self.flow_rate = int(flow_rate)
        self.tunnels = tunnels


pat = re.compile(
    r"Valve ([A-Z]+) has flow rate=(\d+); tunnels? leads? to valves? ((?:[A-Z]+, )*[A-Z]+)")
rooms = dict()

with open("day16.txt") as f:
    for line in f.read().strip().split("\n"):
        name, flow_rate, tunnels = pat.match(line).groups()
        tunnels = tunnels.split(", ")
        rooms[name] = Room(name, flow_rate, tunnels)

interesting_rooms = dict()
for name, room in rooms.items():
    if name == "AA" or room.flow_rate > 0:
        interesting_rooms[name] = room

distances = {name: {name: 0} for name in interesting_rooms}
for name, room in interesting_rooms.items():
    visited = set()
    open = {name}
    while open:
        other = open.pop()
        visited.add(other)
        for adjacent in rooms[other].tunnels:
            if adjacent not in visited:
                open.add(adjacent)
                distances[name][adjacent] = distances[name][other] + 1


def search(current_location, remaining_time, released, opened):
    #print(current_location, remaining_time, released, opened)
    if remaining_time <= 0:
        return released
    current_flow = sum([rooms[name].flow_rate for name in opened])
    if current_location not in opened and rooms[current_location].flow_rate > 0:
        return search(current_location, remaining_time-1,
                      released + current_flow, opened + [current_location])
    elif len(opened) < len(interesting_rooms) - 1:
        possible = []
        for name, room in interesting_rooms.items():
            if name not in opened and room.flow_rate > 0:
                dist = distances[current_location][name]
                if remaining_time > dist:
                    possible.append(search(name, remaining_time-dist,
                                           released + current_flow * dist, opened))
                else:
                    possible.append(current_flow * remaining_time + released)
        return max(possible)
    else:
        return current_flow * remaining_time + released


interesting_rooms = [name for name,
                     room in rooms.items() if room.flow_rate > 0]
possible = []
for perm in permutations(interesting_rooms, 5):
    budget = 30
    prev = "AA"
    release = 0
    for i, room in enumerate(perm):
        if budget > distances[prev][perm[i]]:
            budget -= distances[prev][perm[i]] + 1
            prev = perm[i]
            release += budget * rooms[prev].flow_rate
        else:
            break
    possible.append(release)
print("Part 1:", max(possible))


#print(search("AA", 30, 0, []))
