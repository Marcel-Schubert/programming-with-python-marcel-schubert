class Packet:
    def __init__(self, content: str):
        if content.isdigit():  # potential error source
            self.type = 0  # number
            self.content = int(content)
        else:
            assert content[0] == "["
            assert content[-1] == "]"
            self.type = 1  # list
            self.content = [Packet(child)
                            for child in explode(content[1:-1]) if child]

    def __gt__(self, other):
        if not isinstance(other, Packet):
            print(other)
            raise TypeError
        if self.type == 0 and other.type == 0:
            if self.content == other.content:
                return None
            else:
                return self.content < other.content
        if self.type == 1 and other.type == 1:
            for child, other_child in zip(self.content, other.content):
                if child.__gt__(other_child) is not None:
                    return child.__gt__(other_child)
            if len(self.content) == len(other.content):
                return None
            else:
                return len(self.content) < len(other.content)
        if self.type == 1 and other.type == 0:
            new_other = Packet(f"[{other.content}]")
            return self.__gt__(new_other)
        if self.type == 0 and other.type == 1:
            new_self = Packet(f"[{self.content}]")
            return new_self.__gt__(other)


def explode(content: str):
    nesting_depth = 0
    block_begin = 0
    res = []
    for i, char in enumerate(content):
        if char == "[":
            nesting_depth += 1
        elif char == "]":
            nesting_depth -= 1
        elif char == "," and nesting_depth == 0:
            res.append(content[block_begin:i])
            block_begin = i + 1
    res.append(content[block_begin:])
    return res


with open("day13.txt") as f:
    pairs = f.read().strip().split("\n\n")

part1 = 0
div1 = Packet("[[2]]")
div2 = Packet("[[6]]")
packets = [div1, div2]
for index, pair in enumerate(pairs, start=1):
    left, right = pair.split("\n")
    left = Packet(left)
    right = Packet(right)
    packets.append(left)
    packets.append(right)
    if left > right:
        part1 += index

pos_div1 = sorted(packets)[::-1].index(div1)+1
pos_div2 = sorted(packets)[::-1].index(div2)+1
print(f"Part 1: {part1}")
print(f"Part 2: {pos_div1 * pos_div2}")
