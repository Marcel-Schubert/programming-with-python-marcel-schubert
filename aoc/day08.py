from math import prod
with open("day08.txt") as f:
    grid = [[int(char) for char in line.strip()] for line in f.readlines()]


def visible(height: int, treerow: list[int]) -> bool:
    """Returns true iff a tree of height heigth is visible over the trees in treerow"""
    return all((n < height for n in treerow))


def get_row(row: int) -> list[int]:
    """Return heights in row of grid."""
    return grid[row]


def get_col(col: int) -> list[int]:
    """Return heights in col of grid."""
    return [grid[row][col] for row in range(len(grid))]


def left_of(row, col):
    """Returns heights of trees in row west of col."""
    return get_row(row)[:col][::-1]


def right_of(row, col):
    """Returns heights of trees in row east of col."""
    return get_row(row)[col+1:]


def above(row, col):
    """Returns heights of trees in col north of row."""
    return get_col(col)[:row][::-1]


def below(row, col):
    """Returns heights of trees in col south of row."""
    return get_col(col)[row+1:]


def view_distance(height, treerow):
    """Returns the number of visible trees in treerow."""
    view_distance = 0
    for tree in treerow:
        view_distance += 1
        if tree >= height:
            break
    return view_distance


def scenic_score(height, row, col):
    """Returns the scenic score at position row, col."""
    assert height == grid[row][col]
    return prod((view_distance(height, direction(row, col)) for direction in [left_of, right_of, below, above]))


part1 = part2 = 0
for row, heights in enumerate(grid):
    for col, height in enumerate(heights):
        if any((visible(height, direction(row, col)) for direction in [left_of, right_of, below, above])):
            part1 += 1
        part2 = max(part2, scenic_score(height, row, col))

print(f"Part 1: {part1}")
print(f"Part 2: {part2}")
