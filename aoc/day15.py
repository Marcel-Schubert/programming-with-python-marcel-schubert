import re

with open("day15.txt") as f:
    lines = f.readlines()


def manhattan_dist(x1, y1, x2, y2):
    return abs(x1-x2) + abs(y1-y2)


def covered(pos):
    for pos_sensor, radius in sensors.items():
        if manhattan_dist(*pos, *pos_sensor) <= radius:
            return True
    return False


def in_bounds(num):
    return 0 <= num <= 4_000_000


def candidates():
    for (x, y), radius in sensors.items():
        dist = radius + 1
        for x_next in range(max(0, x - dist), min(x + dist, 4_000_000)):
            y_next = y + (dist - abs(x-x_next))
            if in_bounds(y_next):
                yield (x_next, y_next)
            y_next = y - (dist - abs(x-x_next))
            if in_bounds(y_next):
                yield (x_next, y_next)


sensors = dict()
beacons = set()

pat = re.compile(
    r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)")
for line in lines:
    sensor_x, sensor_y, beacon_x, beacon_y = [
        int(x) for x in pat.match(line).groups()]
    dist = manhattan_dist(sensor_x, sensor_y, beacon_x, beacon_y)
    sensors[(sensor_x, sensor_y)] = dist
    beacons.add((beacon_x, beacon_y))


def tuning_frequency(x, y):
    return x * 4_000_000 + y


def part1():
    res = 0
    y = 2_000_000
    for x in range(min(sensor_x - dist for (sensor_x, _), dist in sensors.items()), max(sensor_x + dist for (sensor_x, _), dist in sensors.items())):
        if covered((x, y)) and (x, y) not in beacons:
            res += 1
    return res


def part2():
    for candidate in candidates():
        if not covered(candidate):
            return tuning_frequency(*candidate)


print(f"Part 1: {part1()}")
print(f"Part 2: {part2()}")
