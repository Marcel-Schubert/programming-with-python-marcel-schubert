with open("day07.txt") as f:
    lines = [line.strip() for line in f.readlines()]


class Directory:
    def __init__(self, name: str, parent):
        self.name = name
        self.parent = parent
        self.subdirs = []
        self.files = []

    def add_file(self, name, size):
        file = File(name, self, size)
        self.files.append(file)

    def add_subdir(self, name):
        directory = Directory(name, self)
        self.subdirs.append(directory)

    def get_subdir(self, name):
        for subdir in self.subdirs:
            if subdir.name == name:
                return subdir

    def size(self):
        file_size = sum((file.size for file in self.files))
        subdir_size = sum((subdir.size() for subdir in self.subdirs))
        return file_size + subdir_size

    def walk_dirs(self):
        yield self
        for subdir in self.subdirs:
            yield from subdir.walk_dirs()

class File:
    def __init__(self, name, parent, size):
        self.name = name
        self.parent = parent
        self.size = size

root = Directory("/", None)
cwd = root
for line in lines:
    match line.split():
        case["$", "cd", ".."]:
            cwd = cwd.parent
        case["$", "cd", "/"]:
            cwd = root
        case["$", "cd", name]:
            cwd = cwd.get_subdir(name)
        case["$", _]:
            # User commands that do "nothing"
            # We need this case to "break" for $ ls to not run into case [size, name]
            pass
        case["dir", name]:
            cwd.add_subdir(name)
        case[size, name]:
            cwd.add_file(name, int(size))


part1 = sum((node.size() for node in root.walk_dirs() if node.size() <= 100_000))
print(f"Part 1: {part1}")

space_needed = 30_000_000
space_total = 70_000_000
space_available = space_total - root.size()
space_to_free = space_needed - space_available

part2 = min((node.size() for node in root.walk_dirs() if node.size() >= space_to_free))
print(f"Part 2: {part2}")
