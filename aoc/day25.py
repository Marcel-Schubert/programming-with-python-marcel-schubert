with open("day25.txt") as f:
    lines = [line.strip() for line in f.readlines()]


def SNAFU_to_decimal(snafu: str) -> int:
    sum = 0
    pos = 0
    for c in snafu[::-1]:
        match c:
            case "=":
                sum += 5**pos * -2
            case "-":
                sum += 5**pos * -1
            case "0":
                sum += 0
            case "1":
                sum += 5**pos * 1
            case "2":
                sum += 5**pos * 2
        pos += 1
    return sum


def decimal_to_SNAFU(num: int) -> str:
    snafu = []
    while num > 0:
        match num % 5:
            case 0:
                snafu.append("0")
            case 1:
                snafu.append("1")
            case 2:
                snafu.append("2")
            case 3:
                snafu.append("=")
                num += 5  # This realizes a carry. After the //5 the number is one lager than it would without
            case 4:
                snafu.append("-")
                num += 5
        num //= 5
    return "".join(snafu[::-1])


total = sum((SNAFU_to_decimal(line) for line in lines))
print("Part 1:", decimal_to_SNAFU(total))
