with open("day12.txt") as f:
    grid = [[c for c in line.strip()] for line in f.readlines()]

for row, line in enumerate(grid):
    if "S" in line:
        col = line.index("S")
        start = (row, col)
        grid[row][col] = "a"
    if "E" in line:
        col = line.index("E")
        end = (row, col)
        grid[row][col] = "z"

grid = [[ord(c) - ord('a') + 1 for c in line] for line in grid]


def adjacent(row, col):
    def in_bounds(row, col):
        return 0 <= row < len(grid) and 0 <= col < len(grid[row])
    res = []
    elevation = grid[row][col]
    for x_offset, y_offset in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        row_c = row + x_offset
        col_c = col + y_offset

        if in_bounds(row_c, col_c) and grid[row_c][col_c] <= elevation + 1:
            res.append((row_c, col_c))
    return res


def bfs(frontier=[(0, start)]):
    closed = set()
    while frontier:
        dist, pos = frontier.pop(0)
        if pos == end:
            return dist
        if pos in closed:
            continue
        closed.add(pos)
        for neighbour in adjacent(*pos):
            if neighbour not in closed:
                frontier.append((dist + 1, neighbour))


print(f"Part 1: {bfs()}")

frontier_p2 = []
for row, line in enumerate(grid):
    for col, c in enumerate(line):
        if c == 1:
            frontier_p2.append((0, (row, col)))

print(f"Part 2: {bfs(frontier_p2)}")
