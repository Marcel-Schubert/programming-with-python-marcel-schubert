from math import lcm

with open("day11.txt") as f:
    monkey_inputs = f.read().split("\n\n")


class Monkey:
    def __init__(self, monkey_input) -> None:
        """Parse monkey input and set variables."""
        for line in monkey_input.splitlines():
            type, arg = line.strip().split(":")
            if type.startswith("Monkey"):
                num = int(type.split(" ")[1])
                self.id = num
            if type == "Starting items":
                items = [int(item) for item in arg.split(",")]
                self.items = items
            if type == "Operation":
                self.expr = arg.split("=")[1]
            if type == "Test":
                match arg.strip().split(" "):
                    case ["divisible", "by", x]:
                        self.divisible_condition = int(x)
                    case _:
                        raise Exception("Unsupported test")
            if type == "If true":
                self.throw_true = int(arg.split(" ")[-1])
            if type == "If false":
                self.throw_false = int(arg.split(" ")[-1])
        self.inspected = 0

    def add_item(self, item):
        """Add item to monkeys items."""
        self.items.append(item)

    def test(self, item):
        """Returns the monkey (by number) the item is thrown to."""
        return self.throw_true if item % self.divisible_condition == 0 else self.throw_false

    def simulate(self, monkeys, keep_managable):
        """Simulate one round for this monkey."""
        while self.items:
            # old looks like it is unused, but it occurs in self.expr and is used in "eval(self.expr)"
            old = self.items.pop(0)
            new = eval(self.expr)
            new = keep_managable(new)
            self.inspected += 1
            monkeys[self.test(new)].add_item(new)

    def __str__(self):
        return f"Monkey {self.id}: {self.items}"


def monkey_business(monkeys):
    """Returns the level of monkey business."""
    inspecteds = sorted([monkey.inspected for monkey in monkeys])
    return inspecteds[-1] * inspecteds[-2]


def part1():
    monkeys = [Monkey(monkey_input) for monkey_input in monkey_inputs]

    for _ in range(20):
        for monkey in monkeys:
            monkey.simulate(monkeys, lambda x: x // 3)

    return monkey_business(monkeys)


def part2():
    monkeys = [Monkey(monkey_input) for monkey_input in monkey_inputs]

    least_common_multiple = lcm(
        *[monkey.divisible_condition for monkey in monkeys])

    for _ in range(10_000):
        for monkey in monkeys:
            monkey.simulate(monkeys, lambda x: x % least_common_multiple)

    return monkey_business(monkeys)


print(f"Part 1: {part1()}")
print(f"Part 2: {part2()}")
