with open("day09.txt") as f:
    instructions = f.readlines()

direction_vectors = {
    "D": lambda x, y: (x, y-1),
    "U": lambda x, y: (x, y+1),
    "L": lambda x, y: (x-1, y),
    "R": lambda x, y: (x+1, y)}


def update_pos_t(num):
    """Moves tail according to the rules."""
    assert num >= 1
    h_x, h_y = positions[num-1]
    t_x, t_y = positions[num]
    # diagonally detatched
    if abs(h_x - t_x) + abs(h_y - t_y) > 2:
        t_x += 1 if h_x > t_x else -1
        t_y += 1 if h_y > t_y else -1
    # horiontally detatched
    elif abs(h_x - t_x) > 1:
        t_x += 1 if h_x > t_x else -1
    # vertically detatched
    elif abs(h_y - t_y) > 1:
        t_y += 1 if h_y > t_y else -1
    positions[num] = (t_x, t_y)
    if num == 1:
        part1.add(positions[num])
    if num == 9:
        part2.add(positions[num])


# The current position of the head and 1 up to 9
positions = [(0, 0)] * 10
# The set of positions visited by tail 1
part1 = {(0, 0)}
# The set of positions visited by tail 9
part2 = {(0, 0)}
for instruction in instructions:
    direction, num = instruction.split()
    num = int(num)
    for _ in range(num):
        positions[0] = direction_vectors[direction](*positions[0])
        for i in range(1, 10):
            update_pos_t(i)


print(f"Part 1: {len(part1)}")
print(f"Part 2: {len(part2)}")
