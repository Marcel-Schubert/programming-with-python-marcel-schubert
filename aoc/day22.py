import re

inp = []
with open("day22.txt") as f:
    for line in f.read().splitlines():
        inp.append(line)

board = inp[:-2]
# Capturing groups in re.split results in keeping the delimiters
instructions = re.split("(L|R)", inp[-1])


def leftmost(row):
    res = 0
    while row[res] not in [".", "#"]:
        res += 1
    return res


def rightmost(row):
    return len(row) - 1


def bottommost(col_index):
    res = len(board) - 1
    while len(board[res]) <= col_index or board[res][col_index] not in [".", "#"]:
        res -= 1
    return res


def topmost(col_index):
    res = 0
    while len(board[res]) <= col_index or board[res][col_index] not in [".", "#"]:
        res += 1
    return res


def keep_in_bound(x, y):
    if facing == 0:  # Right
        if y >= len(board[x]):
            y = leftmost(board[x])
    elif facing == 1:  # Down
        if x >= len(board) or y >= len(board[x]) or board[x][y] == " ":
            x = topmost(y)
    elif facing == 2:  # Right
        if y < 0 or board[x][y] == " ":
            y = rightmost(board[x])
    elif facing == 3:  # Up
        if x < 0 or y >= len(board[x]) or board[x][y] == " ":
            x = bottommost(y)
    return x, y


# 0 := right, 1 := down, 2 := left, 3 := up
directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
# Start at leftmost open tile of the top row of tiles
x, y = 0, board[0].find(".")
# Start facing right
facing = 0

for instruction in instructions:
    if instruction == "R":
        facing = (facing + 1) % 4
    elif instruction == "L":
        facing = (facing - 1) % 4
    else:
        steps = int(instruction)
        for _ in range(steps):
            next_x = x + directions[facing][0]
            next_y = y + directions[facing][1]
            next_x, next_y = keep_in_bound(next_x, next_y)
            if board[next_x][next_y] == "#":
                break
            else:
                x = next_x
                y = next_y

print("Part 1:", 1000*(x+1) + 4*(y+1) + facing)
