with open("day02.txt") as f:
    strat = [line.split() for line in f.readlines()]

scoring = {'A': 1, 'B': 2, 'C': 3, 'X': 1, 'Y': 2, 'Z': 3}
winning = {'A': 'B', 'B': 'C', 'C': 'A'}
losing = {'A': 'C', 'B': 'A', 'C': 'B'}

part1 = 0
for their_symbol, my_symbol in strat:
    part1 += scoring[my_symbol]
    # draw
    if scoring[my_symbol] == scoring[their_symbol]:
        part1 += 3
    # victory
    elif scoring[my_symbol] - 1 == scoring[their_symbol] % 3:
        part1 += 6
    # defeat does not change score

print(f"Part 1: {part1}")

part2 = 0
for their_symbol, outcome in strat:
    # defeat
    if outcome == "X":
        part2 += 0 + scoring[losing[their_symbol]]
    # draw
    elif outcome == "Y":
        part2 += 3 + scoring[their_symbol]
    # victory
    elif outcome == "Z":
        part2 += 6 + scoring[winning[their_symbol]]

print(f"Part 2: {part2}")
