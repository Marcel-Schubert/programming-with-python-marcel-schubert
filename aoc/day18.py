from math import inf

with open("day18.txt") as f:
    lines = f.readlines()


def get_adjacents(p):
    x, y, z = p
    return [(x-1, y, z), (x+1, y, z), (x, y-1, z),
            (x, y+1, z), (x, y, z-1), (x, y, z+1)]


def count_adjacents_in_cubes(p):
    return sum([1 for p2 in get_adjacents(p) if p2 in cubes])


cubes = set()
exposed_sides = 0
for line in lines:
    p = tuple(int(x) for x in line.split(","))
    exposed_sides += 6 - 2 * count_adjacents_in_cubes(p)
    cubes.add(p)

print(f"Part 1: {exposed_sides}")

lower = (min(x for x, y, z in cubes)-1, min(y for x, y,
         z in cubes)-1, min(z for x, y, z in cubes)-1)
upper = (max(x for x, y, z in cubes)+1, max(y for x, y,
         z in cubes)+1, max(z for x, y, z in cubes)+1)


def in_bounds(p):
    x1, y1, z1 = p
    for x2, y2, z2 in cubes:
        if abs(x1 - x2) <= 1 and abs(y1 - y2) <= 1 and abs(z1 - z2) <= 1:
            return True
    return False


# Find point with minimal x.
# We then know that (x-1, y, z) is on the exterior surface.
# This is the starting point for the search.
x_min = inf
for x, y, z in cubes:
    if x < x_min:
        p = (x-1, y, z)
        x_min = x

res = 0
open = {p}
closed = set()
while open:
    p = open.pop()
    if p in closed:
        continue
    closed.add(p)
    res += count_adjacents_in_cubes(p)
    open |= {adj for adj in get_adjacents(p)
             if in_bounds(adj) and adj not in cubes}

print(f"Part 2: {res}")
