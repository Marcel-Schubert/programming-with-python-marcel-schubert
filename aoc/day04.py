with open("day04.txt") as f:
    lines = [[assignments.split("-") for assignments in line.split(",")]
             for line in f.read().splitlines()]

part1 = 0
part2 = 0
for a1, a2 in lines:
    l1, r1 = int(a1[0]), int(a1[1])
    l2, r2 = int(a2[0]), int(a2[1])
    if l1 <= l2 <= r2 <= r1 or l2 <= l1 <= r1 <= r2:
        part1 += 1
    if l1 <= r2 and l2 <= r1:
        part2 += 1

print(f"Part 1: {part1}")
print(f"Part 2: {part2}")
