with open("day05.txt") as f:
    stack_start, instructions = f.read().split("\n\n")
    stack_start = stack_start.split("\n")[:-1]
    instructions = instructions.split("\n")[:-1]


def day05(part1: bool):
    stacks = [[] for _ in range(9)]
    for line in stack_start:
        for i in range(1, len(line), 4):
            if line[i] != ' ':
                stacks[i//4].insert(0, line[i])

    for instruction in instructions:
        _, times, _, fro, _, to = instruction.split()
        times, fro, to = int(times), int(fro) - 1, int(to) - 1
        stacks[fro], chunk = stacks[fro][:-times], stacks[fro][-times:]
        stacks[to] += chunk[::-1] if part1 else chunk

    return "".join([stack[-1] for stack in stacks])


print(f"Part 1: {day05(True)}")
print(f"Part 2: {day05(False)}")
