with open("day03.txt") as f:
    lines = [line.strip() for line in f.readlines()]


def priority(c):
    return ord(c) - ord('A') + 27 if c.isupper() else ord(c) - ord('a') + 1


part1 = 0
for line in lines:
    mid = len(line)//2
    defect = (set(line[:mid]) & set(line[mid:])).pop()
    part1 += priority(defect)

print(f"Part 1: {part1}")

part2 = 0
for e1, e2, e3 in zip(lines[::3], lines[1::3], lines[2::3]):
    badge = (set(e1) & set(e2) & set(e3)).pop()
    part2 += priority(badge)

print(f"Part 2: {part2}")
