from collections import defaultdict
from math import lcm
with open("day17.txt") as f:
    input = f.read().strip()


def jet():
    global pos
    pos = 0
    while True:
        yield 1 if input[pos] == '>' else -1
        pos = (pos + 1) % len(input)


def spawn_positions(type: int, left, bottom):
    if type == 0:
        """####"""
        return [(left+i, bottom) for i in range(4)]
    elif type == 1:
        """ .#.
            ###
            .#."""
        return [(left, bottom+1), (left+1, bottom),
                (left+1, bottom+1), (left+1, bottom+2), (left+2, bottom+1)]
    elif type == 2:
        """ ..#
            ..#
            ###"""
        return [(left, bottom), (left+1, bottom),
                (left+2, bottom), (left+2, bottom+1), (left+2, bottom+2)]
    elif type == 3:
        # Vertical
        return [(left, bottom+i) for i in range(4)]
    elif type == 4:
        return [(left, bottom), (left+1, bottom),
                (left, bottom+1), (left+1, bottom+1)]


class Rock:
    def __init__(self, type: int) -> None:
        left = 2
        bottom = highest() + 4
        self.positions = spawn_positions(type, left, bottom)
        while True:
            self.wind(next(gen))
            if not self.gravity():
                break

    def gravity(self):
        new_positions = [(x, y-1) for x, y in self.positions]
        if any((blocked(pos) for pos in new_positions)):
            for x, y in self.positions:
                board[x].add(y)
            return False
        else:
            self.positions = new_positions
            return True

    def wind(self, direction):
        new_positions = [(x + direction, y) for x, y in self.positions]
        if not any((blocked(pos) for pos in new_positions)):
            self.positions = new_positions


def highest():
    res = 0
    for s in board.values():
        for y in s:
            if y > res:
                res = y
    return res


def blocked(pos):
    """Returns whether a block can move to pos, i.e., pos is out of bounds vertically/horizontally or already blocked by a Rock."""
    x, y = pos
    # Out of bounds
    if y <= 0 or x < 0 or x > 6:
        return True
    # Blocked
    if y in board[x]:
        return True
    return False


def print_board():
    h = highest()
    for y in range(h + 1, 0, -1):
        for i in range(7):
            if y in board[i]:
                print("#", end="")
            else:
                print(".", end="")
        print()
    print()


board = defaultdict(lambda: set())
gen = jet()
combinations_seen = dict()
NUM = 1_000_000_000_000
skipped = False
i = 0
while i < NUM:
    Rock(i % 5)
    if ((i % 5, pos) in combinations_seen) and not skipped:
        last_seen, last_height = combinations_seen[(i % 5, pos)]
        cycle_length = i - last_seen
        if cycle_length >= last_seen:
            combinations_seen[(i % 5, pos)] = (i, highest())
        else:
            cycle_height = highest() - last_height
            cycle_amount = ((NUM - i)//cycle_length)
            skipped_height = cycle_amount * cycle_height
            skipped = True
            i = i + cycle_amount * cycle_length
    else:
        combinations_seen[(i % 5, pos)] = (i, highest())
    i += 1
    if i == 2022:
        print(f"Part 1: {highest()}")

print(f"Part 2: {highest() + skipped_height}")
