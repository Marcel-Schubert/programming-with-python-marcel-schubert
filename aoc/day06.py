with open("day06.txt") as f:
    line = f.read()


def start_of_packet(distincts: int):
    for i, j in zip(range(len(line) - distincts), range(distincts, len(line))):
        if len(set(line[i:j])) == distincts:
            return j


print(f"Part 1: {start_of_packet(4)}")
print(f"Part 2: {start_of_packet(14)}")
