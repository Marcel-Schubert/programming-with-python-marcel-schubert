with open("day10.txt") as f:
    tokens = f.read().split()

x = 1
part1 = 0
crt = []
for cycle, tok in enumerate(tokens, start=1):
    screen_col = (cycle - 1) % 40
    crt.append("#" if abs(screen_col - x) <= 1 else ".")
    if cycle in range(40, len(tokens) + 1, 40):
        crt.append("\n")
    if cycle in range(20, 221, 40):
        part1 += cycle * x
    if "noop" != tok != "addx":
        x += int(tok)


print(f"Part 1: {part1}")
print(f"Part 2:\n{''.join(crt)}")
