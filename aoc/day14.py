with open("day14.txt") as f:
    paths = f.readlines()

rocks = set()
sands = set()


def parse_path(path: str):
    def str_to_tup(point: str):
        return tuple(int(v) for v in point.split(","))
    path = path.split(" -> ")
    for point_a, point_b in zip(path[:-1], path[1:]):
        point_a = str_to_tup(point_a)
        point_b = str_to_tup(point_b)
        if point_a > point_b:
            # If point_a is larger, swap them
            # This has no effect on the next iteration
            point_a, point_b = point_b, point_a
        assert point_a <= point_b
        for i in range(point_a[0], point_b[0] + 1):
            for j in range(point_a[1], point_b[1] + 1):
                rocks.add((i, j))


for path in paths:
    parse_path(path)

# Sand can never be stable below this x-coordinate
max_y = max((y for _, y in rocks))


def day14(part1: bool):
    while (500, 0) not in sands:
        sand_x, sand_y = 500, 0
        while True:
            if sand_y == max_y + 1:
                if part1:
                    return len(sands)
                sands.add((sand_x, sand_y))
                break
            elif (sand_x, sand_y+1) not in rocks and (sand_x, sand_y+1) not in sands:
                sand_y += 1
            elif (sand_x-1, sand_y+1) not in rocks and (sand_x-1, sand_y+1) not in sands:
                sand_x -= 1
                sand_y += 1
            elif (sand_x+1, sand_y+1) not in rocks and (sand_x+1, sand_y+1) not in sands:
                sand_x += 1
                sand_y += 1
            else:
                sands.add((sand_x, sand_y))
                break
    return len(sands)


print(f"Part 1: {day14(part1=True)}")
print(f"Part 2: {day14(part1=False)}")
