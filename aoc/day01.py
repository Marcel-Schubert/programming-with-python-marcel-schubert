with open("./day01.txt") as f:
    lines = [[int(num) for num in line.split()]
             for line in f.read().split("\n\n")]

sums = sorted([sum(line) for line in lines])

print(f"Part 1: {sums[-1]}")
print(f"Part 2: {sum(sums[-3:])}")
