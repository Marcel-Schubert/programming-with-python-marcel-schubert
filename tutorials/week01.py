import math

print("a. 30! mod 59:", math.factorial(30) % 59)
print("b. 2^100 mod 7:", 2**100 % 7)
print("c. the integer part of the division of the integer obtained by concatenating 99 9s, divided by 25:", int("9"*99)//25)
print("d. How many bits are needed to represent 33^33?:", (33**33).bit_length())
print("e. How many digits does the decimal representation of 33^33 have?",
      len(str(33**33)))
print("f. Find lowest and highest value of the absolute values of the numbers −10, 5, 20, −35:",
      min([abs(x) for x in [-10, 5, 20, -35]]), max([abs(x) for x in [-10, 5, 20, -35]]))
print("g. Calculate the area of a circle with radius 3:", math.pi * 3**2)
print("h. Find the number of ±1 walks of 20 steps that start and end in 0, i.e., where in each step, your “position” increases or decreases by 1:", math.comb(20, 10))
n = 3_100_000_000
print("i. The size of the human genome is approx. n = 3 100 000 000. What is log2(n)?", math.log2(n))
